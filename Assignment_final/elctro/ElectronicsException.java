package com.Assignment_final.elctro;

public class ElectronicsException extends RuntimeException{
	private String message;

	public ElectronicsException() {

		this.message = "";
	}

	public ElectronicsException(String message) {

		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public void printStackTrace() {
		System.out.println("Any Gadget not found");
		System.out.println("com.Assignment_final.elctro");
		System.out.println("message="+this.getMessage());
		
	}

}
