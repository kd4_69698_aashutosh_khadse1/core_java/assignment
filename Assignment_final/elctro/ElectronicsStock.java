package com.Assignment_final.elctro;

import java.util.Scanner;

public class ElectronicsStock implements StockManager {
	 int quantity;

	Electronics item;

	@Override
	public void addStock() {

		try {

			this.item.acceptData();
			System.out.println("Enter the quantity");
			this.quantity = new Scanner(System.in).nextInt();

		} catch (Exception e) {
			System.out.println(e);

		}

	}

	@Override
	public void purchaseProduct() {

		if (this.quantity == 0) {
			throw new ElectronicsException("out of stock");
		}

		this.quantity--;
		System.out.println("Remaining Quantity" + this.quantity);

	}

	@Override
	public void displayStock() {
		System.out.println("//_________________________________//");
		this.item.printData();
		System.out.println("quantity = "+ this.quantity);
		System.out.println("//_________________________________//");

	}

}
