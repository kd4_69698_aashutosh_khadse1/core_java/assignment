package  com.Assignment_final.elctro;

import java.util.Scanner;

public abstract class Electronics {
	String model;
	String description;
	double price;
	public void accept()
	{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the model name");
		this.model=sc.next();
		System.out.println("Enter the description");
		this.description=sc.next();
		System.out.println("Enter the price ");
		this.price=sc.nextDouble();
		
	}
	abstract void acceptData() throws MobileException;
	public void print()
	{
		System.out.println("Model Name =  "+this.model);
		System.out.println("Description =   "+this.description);
		System.out.println("Price  =   "+this.price);
	}
	
	abstract void printData();

	@Override
	protected Object clone() throws CloneNotSupportedException {
		 return super.clone();
	}

}
