package com.Assignment_final.elctro;



class MobileException extends Exception {
	private String message;

	public MobileException() {

		this.message = "";
	}

	public MobileException(String message) {

		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public void printStackTrace() {
		System.out.println("Any Device not found");
		System.out.println("com.praffuljain_22.elctro");
		System.out.println("message="+this.getMessage());
		
	}

}
