package com.Assignment_final.elctro;

public interface StockManager  {
	void addStock();
	void purchaseProduct();
	void displayStock();

}
