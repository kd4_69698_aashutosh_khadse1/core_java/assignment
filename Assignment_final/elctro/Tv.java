package com.Assignment_final.elctro;

import java.util.Scanner;

public class Tv extends Electronics{
	int screen_inches;
	int pixel_density;
	
	@Override
	void acceptData() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your tv");
		this.accept();
		System.out.println("Enter the Screen Inches");
		this.screen_inches=sc.nextInt();
		System.out.println("Enter the Pixel Density");
		this.pixel_density=sc.nextInt();
		if(this.pixel_density==0||this.screen_inches==0)
			throw new ElectronicsException(" Pixel density or Screen size cann't be zero ");


		
	}
	@Override
	void printData() {
		this.print();
		System.out.println("Screen Inches  =  "+this.screen_inches);
		System.out.println("Pixel Density  =  "+this.pixel_density);
	
	}

}
